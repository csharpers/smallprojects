﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_MemoryGame
{
    public class SmartValue
    {
        public string Value { get; set; }
        public int Counter { get; set; }

        public SmartValue(string value, int counter)
        {
            this.Value = value;
            this.Counter = counter;
        }
    }
}
