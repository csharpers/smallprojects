﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WPF_MemoryGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SmartLabel FirstSLabel;
        private SmartLabel SecondSLabel;
        private bool IsBothVisible;
        private bool IsPairFound;
        private int Counter;

        public MainWindow()
        {
            InitializeComponent();
            NewGame();
        }

        private void NewGame()
        {
            FirstSLabel = null;
            SecondSLabel = null;
            IsBothVisible = false;
            IsPairFound = false;
            Counter = 0;
            MainGrid.Children.Clear();

            List<SmartValue> smartValues = new List<SmartValue>();
            SmartValue val = null;
            for (int i = 0; i < 8; i++)
            {
                val = new SmartValue(i.ToString(), 0);
                smartValues.Add(val);
            }

            SmartLabel smartLabel = null;
            Random random = new Random();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    smartLabel = new SmartLabel();
                    smartLabel.MouseLeftButtonDown += Lbl_MouseLeftButtonDown;
                    MainGrid.Children.Add(smartLabel);
                    Grid.SetColumn(smartLabel, j);
                    Grid.SetRow(smartLabel, i);

                    bool isSymbolSeted = false;
                    while (!isSymbolSeted)
                    {
                        int lenght = smartValues.Count;
                        if (lenght >= 0)
                        {
                            int index = random.Next(0, lenght);
                            smartLabel.ValueLabel.Content = smartValues[index].Value.ToString();
                            smartValues[index].Counter += 1;

                            if (smartValues[index].Counter == 2)
                            {
                                smartValues.Remove(smartValues[index]);
                            }
                            isSymbolSeted = true;
                        }                        
                    }
                }
            }
        }

        private void Lbl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (IsBothVisible)
            {
                if (!IsPairFound)
                {
                    HideSmartLabel(FirstSLabel);
                    HideSmartLabel(SecondSLabel);
                }
                

                FirstSLabel = SecondSLabel = null;
                IsPairFound = false;
            }

            if (FirstSLabel == null)
            {
                FirstSLabel = sender as SmartLabel;
                DiscoverSmartLabel(FirstSLabel);

                IsBothVisible = false;
            }
            else if (SecondSLabel == null)
            {
                SecondSLabel = sender as SmartLabel;
                DiscoverSmartLabel(SecondSLabel);

                if (FirstSLabel.ValueLabel.Content == SecondSLabel.ValueLabel.Content)
                {
                    FirstSLabel.MouseLeftButtonDown -= Lbl_MouseLeftButtonDown;
                    SecondSLabel.MouseLeftButtonDown -= Lbl_MouseLeftButtonDown;
                    IsPairFound = true;
                    Counter++;
                    if (Counter == 8)
                    {
                        MessageBox.Show("You win!");
                        NewGame();
                        return;
                    }
                }
                IsBothVisible = true;
            }
        }

        private void DiscoverSmartLabel(SmartLabel smartLabel)
        {
            smartLabel.DisplayLabel.Content = smartLabel.ValueLabel.Content;
        }

        private void HideSmartLabel(SmartLabel smartLabel)
        {
            smartLabel.DisplayLabel.Content = "";
        }

        private void BtnNewGame_Click(object sender, RoutedEventArgs e)
        {
            NewGame();
        }
    }
}
